import 'package:chat_gpt/bloc/chat/chat_bloc.dart';
import 'package:chat_gpt/pages/chat/chat_page.dart';
import 'package:chat_gpt/pages/splash/splash_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: Color.fromRGBO(63,182,172, 1), // status bar color
  ));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});


  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => ChatBloc(),
        child: MaterialApp(
          title: 'Chat GPT Flutter',
          theme: ThemeData(
            primaryColor: const Color.fromRGBO(63,182,172, 1)
          ),
          debugShowCheckedModeBanner: false,
          home: const SplashPage(),
        ),
    );
  }
}

