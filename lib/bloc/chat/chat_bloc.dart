import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:chat_gpt/helpers/constant_helper.dart';
import 'package:chat_gpt/models/chat/chat_model.dart';
import 'package:chat_gpt_sdk/chat_gpt_sdk.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'chat_event.dart';
part 'chat_state.dart';

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  ChatBloc() : super(ChatState.initial()) {
    on<ChatInitialEvent>(_handleChatInitial);
    on<SendMessageEvent>(_handleSendMessage);
  }

  Future<void> _handleChatInitial(
      ChatInitialEvent event, Emitter<ChatState> emit) async {
    OpenAI? initChatGPT;
    initChatGPT = OpenAI.instance.build(
        token: ConstantHelper.apiKey,
        baseOption: HttpSetup(receiveTimeout: 20000));

    emit(state.copyWith(
        chatGPT: initChatGPT, chatStatus: ChatStateStatus.initial));
  }

  Future<void> _handleSendMessage(
      SendMessageEvent event, Emitter<ChatState> emit) async {
    try {
      if (event.message.isNotEmpty) {
        int sizeScrollController = 0;
        List<ChatModel> listResultChat = state.listChatMessages;
        listResultChat.add(ChatModel(
            sender: ConstantHelper.humanSender, textMessage: event.message));

        sizeScrollController = getSizeScrollController(listResultChat);
        emit(state.copyWith(
            isTyping: true,
            listChatMessages: listResultChat,
            chatStatus: ChatStateStatus.sendMessage));

        final requestMessage =
            CompleteText(prompt: event.message, model: kTranslateModelV3);
        final responseBotMessage =
            await state.chatGPT!.onCompleteText(request: requestMessage);

        listResultChat.add(ChatModel(
            sender: ConstantHelper.botSender,
            textMessage: responseBotMessage!.choices[0].text));

        sizeScrollController = getSizeScrollController(listResultChat);
        emit(state.copyWith(
            isTyping: false,
            chatStatus: ChatStateStatus.successfullySendMessage,
            sizeScrollController: sizeScrollController,
            listChatMessages: listResultChat));
      }
    } catch (e) {
      List<ChatModel> listChatMessage = state.listChatMessages;
      listChatMessage.removeLast();
      debugPrint('Error send message $e');
      emit(state.copyWith(
          isTyping: false,
          chatStatus: ChatStateStatus.failedSendMessage,
          listChatMessages: listChatMessage));
    }
  }

  int getSizeScrollController(List listChatMessage) {
    int size = 0;

    if (listChatMessage.length > 6 && listChatMessage.length < 8) {
      size = 150;
    } else if (listChatMessage.length >= 8 && listChatMessage.length < 10) {
      size = 200;
    } else if (listChatMessage.length >= 10) {
      size = 300;
    }

    return size;
  }
}
