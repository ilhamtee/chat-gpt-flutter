part of 'chat_bloc.dart';

enum ChatStateStatus {
  initial,
  sendMessage,
  successfullySendMessage,
  failedSendMessage,
}

class ChatState {
  final ChatStateStatus chatStatus;
  final List<ChatModel> listChatMessages;
  final bool isTyping;

  final OpenAI? chatGPT;
  final int sizeScrollController;

  factory ChatState.initial() => ChatState(
      sizeScrollController: 0,
      chatStatus: ChatStateStatus.initial,
      isTyping: false,
      listChatMessages: []);

  ChatState(
      {required this.listChatMessages,
      required this.isTyping,
      required this.chatStatus,
      required this.sizeScrollController,
      this.chatGPT});

  ChatState copyWith(
      {ChatStateStatus? chatStatus,
      List<ChatModel>? listChatMessages,
      bool? isTyping,
      int? sizeScrollController,
      OpenAI? chatGPT}) {
    return ChatState(
        chatStatus: chatStatus ?? this.chatStatus,
        listChatMessages: listChatMessages ?? this.listChatMessages,
        isTyping: isTyping ?? this.isTyping,
        sizeScrollController: sizeScrollController ?? this.sizeScrollController,
        chatGPT: chatGPT ?? this.chatGPT);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ChatState &&
          runtimeType == other.runtimeType &&
          sizeScrollController == other.sizeScrollController &&
          chatStatus == other.chatStatus &&
          listChatMessages == other.listChatMessages &&
          isTyping == other.isTyping &&
          chatGPT == other.chatGPT;

  @override
  int get hashCode =>
      sizeScrollController.hashCode ^
      chatStatus.hashCode ^
      listChatMessages.hashCode ^
      isTyping.hashCode ^
      chatGPT.hashCode ^
      sizeScrollController.hashCode;
}
