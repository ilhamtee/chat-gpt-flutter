part of 'chat_bloc.dart';

abstract class ChatEvent extends Equatable {}

class ChatInitialEvent extends ChatEvent{
  @override
  List<Object?> get props => [];

}

class SendMessageEvent extends ChatEvent{
  final String message;

  SendMessageEvent({required this.message});

  @override
  String toString() => 'SendMessageEvent{message: $message}';

  @override
  List<Object?> get props => [];

}