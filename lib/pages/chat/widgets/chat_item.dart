import 'package:chat_gpt/helpers/constant_helper.dart';
import 'package:chat_gpt/models/chat/chat_model.dart';
import 'package:flutter/material.dart';

class ChatItem extends StatelessWidget {
  final ChatModel data;
  const ChatItem({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (data.sender == ConstantHelper.humanSender) {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Flexible(
            child: Container(
              margin: const EdgeInsets.only(left: 150, right: 10,top: 3),
              padding: const EdgeInsets.all(15),
              decoration: const BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15))),
              child: Text(
                data.textMessage,
                style: const TextStyle(color: Colors.white),
                maxLines: null,
              ),
            ),
          ),
          const CircleAvatar(
            radius: 20,
            backgroundColor: Colors.green,
            child: Text('YOU', style: TextStyle(
              color: Colors.white,
              fontSize: 13,
              fontWeight: FontWeight.bold
            ),),
          )
        ],
      );
    }

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        const CircleAvatar(
          radius: 20,
          backgroundColor: Colors.white,
          child: Text('BOT', style: TextStyle(
              color: Colors.black,
              fontSize: 13,
              fontWeight: FontWeight.bold
          ),),
        ),
        Flexible(
          child: Container(
            margin: const EdgeInsets.only(right: 100, left: 5, top: 3),
            padding: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(15),
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15))),
            child: Text(
              data.textMessage,
              style: const TextStyle(color: Colors.black),
            ),
          ),
        )
      ],
    );
  }
}
