import 'package:chat_gpt/bloc/chat/chat_bloc.dart';
import 'package:chat_gpt/models/chat/chat_model.dart';
import 'package:chat_gpt/pages/chat/widgets/chat_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:chat_gpt_sdk/chat_gpt_sdk.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  late ChatBloc _chatBloc;

  final TextEditingController _messageChatController = TextEditingController();
  final ScrollController _scrollChatController = ScrollController();

  @override
  void initState() {
    super.initState();
    _chatBloc = context.read<ChatBloc>();
    _chatBloc.add(ChatInitialEvent());
  }

  @override
  void dispose() {
    _messageChatController.dispose();
    _scrollChatController.dispose();
    _chatBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<ChatBloc, ChatState>(
        bloc: _chatBloc,
        listener: (context, state) {
          switch (state.chatStatus) {
            case ChatStateStatus.initial:
              break;
            case ChatStateStatus.successfullySendMessage:
              _scrollChatController.animateTo(
                _scrollChatController.position.maxScrollExtent + state.sizeScrollController,
                duration: const Duration(milliseconds: 500),
                curve: Curves.easeOut,
              );
              break;
            case ChatStateStatus.failedSendMessage:
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                content: Text(
                  'Failed to send message, Please try again!',
                  style: TextStyle(fontSize: 14),
                ),
                backgroundColor: Colors.redAccent,
                duration: Duration(seconds: 3),
              ));
              break;
            case ChatStateStatus.sendMessage:
              _messageChatController.clear();
              _scrollChatController.animateTo(
                _scrollChatController.position.maxScrollExtent + state.sizeScrollController,
                duration: const Duration(milliseconds: 500),
                curve: Curves.easeOut,
              );
              break;
          }
        },
        builder: (context, state) {
          return SafeArea(
            child: Stack(
              children: [
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height,
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          image:
                              AssetImage('assets/images/background_chat.png'),
                          fit: BoxFit.cover)),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 10, bottom: 80),
                  child: ListView.builder(
                      controller: _scrollChatController,
                      shrinkWrap: true,
                      itemCount: state.listChatMessages.length,
                      itemBuilder: (context, index) {
                        var chat = state.listChatMessages[index];
                        return Container(
                          margin: const EdgeInsets.symmetric(
                              vertical: 15, horizontal: 10),
                          alignment: chat.sender == '1'
                              ? Alignment.centerRight
                              : Alignment.centerLeft,
                          child: ChatItem(data: chat),
                        );
                      }),
                ),
                Positioned(
                    bottom: 0,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Expanded(
                              child: Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20),
                            constraints: const BoxConstraints(
                              maxHeight: 300
                            ),
                            decoration: BoxDecoration(
                                color: Colors.grey[800],
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(20))),
                            child: TextFormField(
                              style: const TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              controller: _messageChatController,
                              maxLines: null,
                              minLines: 1,
                              decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Let's talk with GPT ...",
                                  hintStyle: TextStyle(color: Colors.white)),
                            ),
                          )),
                          const SizedBox(width: 10),
                          GestureDetector(
                            onTap: () {
                              if (_messageChatController.text.isNotEmpty &&
                                  !state.isTyping) {
                                _chatBloc.add(SendMessageEvent(
                                    message: _messageChatController.text));
                              }
                            },
                            child: Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                  color: state.isTyping
                                      ? Colors.grey
                                      : Theme.of(context).primaryColor, shape: BoxShape.circle),
                              alignment: Alignment.center,
                              child: state.isTyping
                                  ? const SizedBox(
                                width: 25,
                                height: 25,
                                child: CircularProgressIndicator(
                                  color: Colors.white,
                                  backgroundColor: Colors.grey,
                                ),
                              )
                                  : const Icon(
                                Icons.send,
                                color: Colors.white,
                                size: 30,
                              ),
                            ),
                          )
                        ],
                      ),
                    )),
                state.isTyping
                    ? Positioned(
                        bottom: 95,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.green.withOpacity(0.5),
                          padding: const EdgeInsets.symmetric(vertical: 2),
                          alignment: Alignment.center,
                          child: const Text(
                            'Bot GPT is typing ...',
                            style: TextStyle(
                                color: Colors.white,
                                fontStyle: FontStyle.italic),
                            textAlign: TextAlign.center,
                          ),
                        ))
                    : const SizedBox()
              ],
            ),
          );
        },
      ),
    );
  }
}
