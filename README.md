# CHAT GPT Using Flutter

<img src = "assets/images/chat_gpt.png">
<p>
A Chatbot chat app built using the Flutter framework and OpenAI's GPT-3 language model.
</p>

## Features

- Natural language understanding
- Human-like conversation
- Customizable to fit your use-case
- Lightweight, easy to integrate with other apps
- Open-source

## Prerequisites

- Flutter SDK
- API key for OpenAI GPT-3 [Available here](https://beta.openai.com/account/api-keys)
- chat_gpt_sdk:
  git:
  url: https://github.com/iampawan/Flutter-ChatGPT.git

## Getting Started

1. Clone the repository
2. Run `flutter pub get` to install dependencies
3. Replace the placeholder API key with your own in the `contant_helper.dart` file
4. Run the app on an emulator or physical device
